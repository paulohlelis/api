<?php

namespace App\Providers;

use App\Usuario;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Response;
use \Firebase\JWT\JWT;
use Illuminate\Support\Facades\Session;

class AuthServiceProvider extends ServiceProvider
{
    // até agora nao consegui tirar ISSO de uma CONST
    CONST PUBLIC_KEY = "-----BEGIN CERTIFICATE-----\nMIIC+jCCAeKgAwIBAgIIHubY78Nv5kQwDQYJKoZIhvcNAQEFBQAwIDEeMBwGA1UE\nAxMVMTE0MDMzODIxMzg2NTIwMTY0ODQ4MB4XDTE3MDkxNTE5MzEzOVoXDTI3MDkx\nMzE5MzEzOVowIDEeMBwGA1UEAxMVMTE0MDMzODIxMzg2NTIwMTY0ODQ4MIIBIjAN\nBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlTDBVPJ5K8F/cF8Excz1DcdvjqeG\ndKPQlkCRY+1IBap5DsV3U4akcrg94TdFjhnCWpBNzfbZF97TFWOhHPFOwipGPNGH\nRKSutV4o5PcAYlWunIY04U6weC4GdqKlxbkG9ObUK7gifSNQpqP4qp93LpGDWiEr\n1ThDkAPpmK/B7LKf12CHj7M2kaN8+uUXuf/OAjG1QnbDLyoBxxtpWZehHwob110b\nqVLEnjrUBBJskSZrcPJ+LMDGRkVZQeD56bSTlmyn/oR5eWI0nRBiDptUDAP7JOyf\nT1NPYfVqIFjdn5LvVECn3M8dzQ5bxIvCokD5wAEPFmCWpKuhmGZzjbf+kQIDAQAB\nozgwNjAMBgNVHRMBAf8EAjAAMA4GA1UdDwEB/wQEAwIHgDAWBgNVHSUBAf8EDDAK\nBggrBgEFBQcDAjANBgkqhkiG9w0BAQUFAAOCAQEADqUtOPSrZKMUy+FKh+AboNDr\nUP2pNkTk27M+MqJNu18CljVbKA39EMHRw86OZfU8su4ycY+chdorFas5eeBUBIa8\nTHcDR89CJTJyGdhGGKGYOr5tm/JxLp5Aeq/grSIFOWMSL5ZiYzjUaYRGnpzjXEU3\n0HF0B2YF10SsEwVmTXZ0qzacvBoDt7tGEaxgn1ZrJlTsvxr4QExHHk5ZMzmWPGz9\ndJfBT296LnwGqeXTjfy6apdF5sZ7IqFKKorgEOVlj1nYm+uTVUFiolK4DAmf4exi\nceiPo7YjoGsoYrvBsG3fIq5lEbz9l62z2G0W1kyJdufr1UpQzhY8mo0seA49pA==\n-----END CERTIFICATE-----\n";    

    public function register()
    {
        //
    }

    public function boot()
    {

        $this->app['auth']->viaRequest('api', function ($request) {

            // Verifica se existe o Authorization header da requisição se nao tiver retorna (UNAUTHORIZED)
            if( $request->header('Authorization') != null ){
                $token = $request->header('Authorization');

                // Entra apenas se o token nao estiver vazio se tiver retorna null  (UNAUTHORIZED)
                if(!empty($token)){
                    // Aqui tenta decodificar o TOKEN se falhar em decodificar retorna FALSE (UNAUTHORIZED)
                    try{
                        $token = JWT::decode($token, $this::PUBLIC_KEY, array('RS256'));
                    } catch (\Firebase\JWT\SignatureInvalidException $ex) {
                        $token = false;
                    } catch(\Firebase\JWT\ExpiredException $ex) {
                        $token = false;
                    }
                    
                    // Se o TOKEN tiver algum valor continua
                    if($token){
                        // Pega o usuário pelo ID do TOKEN que está no UID
                        $user = Usuario::where('id', $token->uid)->first();

                        if (is_null($user)) {
                            return null;
                        }
                        // Pega o email do token
                        $email = $token->claims->email;

                        // Verifica se o email do token é o mesmo do usuário no banco se for retorna o usuário se nao retorna null (UNAUTHORIZED)
                        if ($email === $user->email) {
                            return $user;
                        } else {
                            return null;                            
                        }
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            }else{
                return null;
            }
        });
    }
}
