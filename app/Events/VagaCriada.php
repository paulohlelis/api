<?php
namespace App\Events;

use App\Vaga;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class VagaCriada extends Event implements ShouldBroadcast
{
   
    public $vaga;
    public $republica;
    public $imagens;
    public $conveniencias;
    public $telefones;
    public $usuario;

    public function __construct(Vaga $vaga)
    {
        $vagaToSend = Vaga::where('id', $vaga->id)->with('republica', 'republica.usuario', 'republica.imagens', 'republica.conveniencias', 'republica.telefones')->first();
        $this->vaga = $vagaToSend;
        $this->republica = $vagaToSend->republica;
        $this->usuario = $vagaToSend->republica->usuario;
        $this->imagens = $vagaToSend->republica->imagens;
        $this->conveniencias = $vagaToSend->republica->conveniencias;
        $this->telefones = $vagaToSend->republica->telefones;
    }

    public function broadcastOn()
    {
        return ['republica_vagas'];
    }
}