<?php
namespace App\Events;

use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Republica;

class MoradorAdicionado extends Event implements ShouldBroadcast {

    public $republica;
    public $moradorId;

    public function __construct(Republica $republica, $moradorId) {
        $this->republica = $republica;
        $this->moradorId = $moradorId;
    }

    public function broadcastOn() {
        return ['usuario_'.$this->moradorId];
    }
}
