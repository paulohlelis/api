<?php
namespace App\Events;

use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UsuarioEmailVerificado extends Event implements ShouldBroadcast {

    public $status;
    public $usuarioId;

    public function __construct($status, $usuarioId) {
        $this->status = $status;
        $this->usuarioId = $usuarioId;
    }

    public function broadcastOn() {
        return ['usuario_'.$this->usuarioId];
    }
}
