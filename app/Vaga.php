<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Vaga extends Model {

    protected $fillable = ["titulo", "valor", "tipo_de_quarto", "republica_id", "vaga_garagem"];

    protected $dates = [];

    public static $rules = [
        "titulo" => "required|string",
        "valor" => "required|numeric",
        "tipo_de_quarto" => "required|string",
        "republica_id" => "required|string",
        "vaga_garagem" => "boolean"
    ];

    public $timestamps = true;

    public function republica()
    {
        return $this->belongsTo("App\Republica");
    }

    public function usuarios()
    {
        return $this->belongsToMany("App\Usuario")->withPivot('notificationId', 'situacao')->withTimestamps();
    }

    public function visitas()
    {
        return $this->hasMany("App\Visita");
    }

    public function despesas()
    {
        return $this->belongsToMany("App\Despesa");
    }


}
