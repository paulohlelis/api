<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Endereco extends Model {

    protected $fillable = [ "cep", "logradouro", "numero", "complemento_id", "numeroApto", "bairro_id", "end_lat", "end_lng"];

    protected $dates = [];

    public static $rules = [
        "cep" => "required|digits:8",
        "logradouro" => "required|string",
        "numero" => "required|numeric",
        "complemento_id" => "nullable",
        "numeroApto" => "nullable",
        "bairro_id" => "required|numeric",
        "end_lat" => "required",
        "end_lng" => "required"
    ];

    public $timestamps = false;

    public function universidades()
    {
        return $this->hasMany("App\Universidade");
    }

    public function bairro()
    {
        return $this->belongsTo("App\Bairro");
    }

    public function complemento()
    {
        return $this->belongsTo("App\Complemento");
    }

    public function republicas()
    {
        return $this->hasMany("App\Republica");
    }


}
