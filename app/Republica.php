<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Traits\UuidsTrait;

class Republica extends Model
{

    use UuidsTrait;

    protected $fillable = [
        "nomeRepublica",
        "curso",
        "quantidadeQuartos",
        "quantidadeMoradores",
        "descricao",
        "tipoRepublica",
        "universidade_id",
        "endereco_id",
        "usuario_id"
    ];

    public $incrementing = false;
    
    protected $dates = ['created_at', 'updated_at'];

    public static $rules = [
        "nomeRepublica" => "required|string",
        "quantidadeQuartos" => "required",
        "tipoRepublica" => "required",
        "endereco_id" => "required|numeric",
        "usuario_id" => "required|string",
    ];

    public $timestamps = true;


    public function vagas()
    {
        return $this->hasMany("App\Vaga");
    }

    public function imagens()
    {
        return $this->hasMany("App\Imagem");
    }

    public function telefones()
    {
        return $this->hasMany("App\Telefone");
    }

    public function universidade()
    {
        return $this->belongsTo("App\Universidade");
    }

    public function endereco()
    {
        return $this->belongsTo("App\Endereco");
    }

    // Owner
    public function usuario()
    {
        return $this->belongsTo("App\Usuario");
    }

    public function moradores()
    {
        return $this->hasMany("App\Usuario");
    }

    public function conveniencias()
    {
        return $this->belongsToMany("App\Conveniencia")->withTimestamps();
    }

    public function getMapsInfo()
    {
        $republica = $this;

        $endereco = $this->endereco()->first();

        $conveniencias = $republica->conveniencias()->get()->pluck('descricao');

        $universidade = $republica->universidade()->first();

        $telefones = $republica->telefones()->get()->pluck('numeroTelefone');

        $coordinates = array('lat' => $endereco->end_lat, 'lng' => $endereco->end_lng );

        $mapInfo = array(
            'republica' => $republica,
            'telefones' => $telefones,
            'universidade' => $universidade,
            'conveniencias' => $conveniencias,
            'coordenadas' => $coordinates
        );

        return $mapInfo;
    }
}
