<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Despesa extends Model {

    protected $fillable = ["descricao"];

    protected $dates = [];

    public static $rules = [
        "descricao" => "required",
    ];

    public $timestamps = false;

    // Relationships
    public function vagas()
    {
        return $this->belongsToMany("App\Vaga");
    }

}
