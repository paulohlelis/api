<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conveniencia extends Model {

    protected $fillable = ["descricao"];

    protected $dates = [];

    public static $rules = [
        "descricao" => "required|string",
    ];

    public $timestamps = false;

    public function republicas()
    {
        return $this->belongsToMany("App\Republica")->withTimestamps();
    }


}
