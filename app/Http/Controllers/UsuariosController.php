<?php
namespace App\Http\Controllers;

// use Lcobucci\JWT\Builder;
// use Lcobucci\JWT\Signer\Keychain;
// use Lcobucci\JWT\Signer\Rsa\Sha256;
use \Firebase\JWT\JWT;

use Illuminate\Hashing\BcryptHasher;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Event;
use Auth;
use App\Events\UsuarioEmailVerificado;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use App\Consts\Notification;
use App\TipoAuth;
use App\Imagem;
use App\TipoTelefone;
use App\Vaga;
use App\Usuario;
use App\Republica;
use App\Telefone;
use Illuminate\Support\Facades\Session;

class UsuariosController extends Controller
{

    const MODEL = "App\Usuario";
    const TEL = "App\Telefone";
    const VAGA = "App\Vaga";
    const TIPOAUTH = "App\TipoAuth";
    const TIPOTEL = "App\TipoTelefone";

    use RESTActions, NotificationTrait;

    const PRIVATE_KEY = "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCVMMFU8nkrwX9w\nXwTFzPUNx2+Op4Z0o9CWQJFj7UgFqnkOxXdThqRyuD3hN0WOGcJakE3N9tkX3tMV\nY6Ec8U7CKkY80YdEpK61Xijk9wBiVa6chjThTrB4LgZ2oqXFuQb05tQruCJ9I1Cm\no/iqn3cukYNaISvVOEOQA+mYr8Hssp/XYIePszaRo3z65Re5/84CMbVCdsMvKgHH\nG2lZl6EfChvXXRupUsSeOtQEEmyRJmtw8n4swMZGRVlB4PnptJOWbKf+hHl5YjSd\nEGIOm1QMA/sk7J9PU09h9WogWN2fku9UQKfczx3NDlvEi8KiQPnAAQ8WYJakq6GY\nZnONt/6RAgMBAAECggEAAxqT28ayI5QHs4Cp5qFrmT0UlUhnX+qZurXZQysS+CQ1\nFyFfqMi+B9wiVHAhZdy5FN2C8uBPdq4g9dqquyGK6LeiBQlRFJS5BPFdLYX1LPaO\nTA0aO/RBRV95bLMtpU9ixXT+cHI+THHFRNGX6S5+t3ounDEhD8iXK5Tl7Ee+zk6b\nvbDusy/MQhWy6d2DPeipC5QDTE97l9oEvfvnMRN2zKHmjNIK9kGZhArqg6ic2I8K\nPyIyXxqhAGAAOSHGbIaL/SXoozc7/DNIFMIzxWA3EMwXDJuZH/m4y0DqVSRJXoQI\notkl91SYX92rFl82KdKYDTrz55VWxGGYSbbpBdXYQQKBgQDMprfTYWIDsQKIOty0\n7ZUN5pf8aDnoa732xI8dHZR6+v7YmPD9L17uVXf0hFsHqObDiMqlimVJKyMYWy/F\nFFwbShpfV+Ubj/XdZZgWZWB517Ndc2fi4ePhBg088ypKwp/pCv/7EhO/uaoiek2s\n9Kg3rPp1lOs8eE+xVQTwfnpZYQKBgQC6n6VmPzM8/dFH0ihM4Hp/2Y+3RilQhMSn\nqYAFfCQ3v5payhbC6WgQFkBi5FKrRsJbMtrTUAyISOA4+Yfponn9Y8FwBHyEA/Td\ntNLEC84szsNxbnYWG2306GOEZxs3iFPvEMEo31+uAVvUlOf7ttOmz+DHu/o8ETUE\ndaGqOTjDMQKBgQDKtklX5BpGNcugTU3ZGYyPXF4n8sWJZ0+fY0uvN2Gf/4b33Wa4\nI3gQ9DAB0CirCa6q+JBiMRfqxgPZWFmVCXnCvkApONMD3jFXwA5KANjzV6mfY3T/\n02j5uBjcyrCVpProx7CPbXSKcMoJpMPUEfOeUoU0evIXz8SAjwp5fY6LgQKBgQCA\nsSB0rrZdo8YSmdiSkuXGjjetsaNB1BxsZdVTcvTiwKznWJ5zkFLjbQS/bSWY2LWk\nACCoQXYy8NY2U9SmhxVKZ+T3oxBeVQAhdtU0vdNSUM6zxPGHcyjTDwG5RwScudTv\ncYVR8x9eta8mm/yRNtUjHgyqj3USwObniVqPMOB0sQKBgEd0XXoOfPjxhIPP/ewG\n2pdbNldGPbGd+UABe4/kUxsxJMwNlzSQaisGdQulaICwRSHmt6J3q3JXX2J+OyDK\nA85Jr78xB3sHLe5POljxqyRRExoIZCq7skk+wvksVoYbmIgu3z+vn/o1DkcClFGB\nkVjYwuQQKKIy9CI3YrBAATbM\n-----END PRIVATE KEY-----\n";
    const PUBLIC_KEY = "-----BEGIN CERTIFICATE-----\nMIIC+jCCAeKgAwIBAgIIHubY78Nv5kQwDQYJKoZIhvcNAQEFBQAwIDEeMBwGA1UE\nAxMVMTE0MDMzODIxMzg2NTIwMTY0ODQ4MB4XDTE3MDkxNTE5MzEzOVoXDTI3MDkx\nMzE5MzEzOVowIDEeMBwGA1UEAxMVMTE0MDMzODIxMzg2NTIwMTY0ODQ4MIIBIjAN\nBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlTDBVPJ5K8F/cF8Excz1DcdvjqeG\ndKPQlkCRY+1IBap5DsV3U4akcrg94TdFjhnCWpBNzfbZF97TFWOhHPFOwipGPNGH\nRKSutV4o5PcAYlWunIY04U6weC4GdqKlxbkG9ObUK7gifSNQpqP4qp93LpGDWiEr\n1ThDkAPpmK/B7LKf12CHj7M2kaN8+uUXuf/OAjG1QnbDLyoBxxtpWZehHwob110b\nqVLEnjrUBBJskSZrcPJ+LMDGRkVZQeD56bSTlmyn/oR5eWI0nRBiDptUDAP7JOyf\nT1NPYfVqIFjdn5LvVECn3M8dzQ5bxIvCokD5wAEPFmCWpKuhmGZzjbf+kQIDAQAB\nozgwNjAMBgNVHRMBAf8EAjAAMA4GA1UdDwEB/wQEAwIHgDAWBgNVHSUBAf8EDDAK\nBggrBgEFBQcDAjANBgkqhkiG9w0BAQUFAAOCAQEADqUtOPSrZKMUy+FKh+AboNDr\nUP2pNkTk27M+MqJNu18CljVbKA39EMHRw86OZfU8su4ycY+chdorFas5eeBUBIa8\nTHcDR89CJTJyGdhGGKGYOr5tm/JxLp5Aeq/grSIFOWMSL5ZiYzjUaYRGnpzjXEU3\n0HF0B2YF10SsEwVmTXZ0qzacvBoDt7tGEaxgn1ZrJlTsvxr4QExHHk5ZMzmWPGz9\ndJfBT296LnwGqeXTjfy6apdF5sZ7IqFKKorgEOVlj1nYm+uTVUFiolK4DAmf4exi\nceiPo7YjoGsoYrvBsG3fIq5lEbz9l62z2G0W1kyJdufr1UpQzhY8mo0seA49pA==\n-----END CERTIFICATE-----\n";
    
    public function __construct()
    {
    }

    private function encrypt_decrypt($acao, $string) {
        $output = false;

        $encrypt_method = "AES-256-CBC";
        $secret_key = env("APP_SECRET");
        $secret_iv = env("APP_KEY");

        $key = hash("sha256", $secret_key);

        $iv = substr(hash("sha256", $secret_iv), 0, 16);

        if ($acao == "encrypt") {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if ($acao == "decrypt") {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }

    public function requestVerificationEmail(Request $request) {
        $email = $request->input("email");
        $usuario = Usuario::where("email", "=", $email)->first();
        $verification = array("usuario_id" => $usuario->id, "email" => $usuario->email);
        $json = json_encode($verification);
        $token = $this->encrypt_decrypt("encrypt", $json);

        try {
            Mail::send("verify", ["token" => $token, "email" => $email], function($message) use ($email, $usuario) {
                $message->to($email, $usuario->nome)->subject("Verifique seu email.");
            });
            return $this->respond(Response::HTTP_OK, ["status" => "Email enviado. Verifique sua caixa de email."]);
        } catch (Exception $ex) {
            return $this->respond(Response::HTTP_INTERNAL_SERVER_ERROR, ["status" => "Não foi possível enviar o email."]);
        }
        
    }


    public function verifyEmail(Request $request) {
        $token = $request->input("token");

        $decrypted = $this->encrypt_decrypt("decrypt", $token);

        $verification = json_decode($decrypted);

        if (is_null($verification->usuario_id)) {
            header("Location: http://administrador.republicando.esy.es/#/emailVerified?status=".false);
        }

        $usuario = Usuario::find($verification->usuario_id);
        if ($usuario->email !== $verification->email) {
            header("Location: http://administrador.republicando.esy.es/#/emailVerified?status=".false);       
        }

        $usuario->email_verified = true;
        $usuario->save();
        Event::fire(new UsuarioEmailVerificado(true, $usuario->id));
        header("Location: http://administrador.republicando.esy.es/#/emailVerified?status=".true);
    }

    public function getComTelefones($id)
    {
        $m = $this::MODEL;

        return $m::with("telefones")->find($id)->telefones;
    }

    public function cadastrar(Request $request)
    {

        $this->validate($request, Usuario::$rules, Usuario::$messages);

        $telefones = $request["telefones"];

        $request["password"] = app("hash")->make($request["password"]);

        $tipo = TipoAuth::where("descricao", "email_password")->first();

        $request["tipoAuth_id"] = $tipo->id;

        $user = Usuario::create($request->all());

        $user->imagens()->create([
            "url" => "https://www.gravatar.com/avatar/".md5($user->email)."?s=140"
        ]);
        
        if (!empty($telefones)) {
            //Salva os telefones do usuario 1 ou muitos
            $user->telefones()
            ->save(Telefone::create([
                "numeroTelefone" => $telefones["numeroTelefone"],
                "tipoTelefone_id" => $telefones["idTipoTelefone"]
            ]));
        }

        $user->notificationToken = $request["notificationToken"];

        $user->refreshTokenId = $this->generateRefreshTokenId();

        $user->save();

        $user = Usuario::where("id", $user->id)->with("imagens", "telefones", "republica")->first();

        $token = $this->generateToken($user);

        return $this->respond(Response::HTTP_OK, ["usuario" => $user, "token" => $token]);
    }

    public function login(Request $request)
    {

        if (!$request["email"] || !$request["password"]) {
            return $this->respond(Response::HTTP_INTERNAL_SERVER_ERROR, ["error" => "Email e/ou senha não foram informados."]);
        }
        $user = Usuario::where("email", $request["email"])->with("telefones", "imagens", "republica")->first();

        if (!$user) {
            return $this->respond(Response::HTTP_INTERNAL_SERVER_ERROR, ["error" => "Este email não está cadastrado em nosso sistema."]);
        }

        if (!app("hash")->check($request["password"], $user->password)) {
            return $this->respond(Response::HTTP_INTERNAL_SERVER_ERROR, ["error" => "Sua senha está incorreta."]);
        }

        $user->notificationToken = $request["notificationToken"];

        $user->refreshTokenId = $this->generateRefreshTokenId();

        $user->save();

        $token = $this->generateToken($user);
        
        return $this->respond(Response::HTTP_OK, ["usuario" => $user, "token" => $token, "refreshTokenId" => $user->refreshTokenId ]);
    }

    public function loginAdmin(Request $request)
    {
        $m = $this::MODEL;

        if (!$request["email"] || !$request["password"]) {
            return $this->respond(Response::HTTP_INTERNAL_SERVER_ERROR, ["error" => "Email e/ou senha não informados."]);
        }

        $user = $m::where("email", $request["email"])
        ->first();

        if (!$user) {
            return $this->respond(Response::HTTP_INTERNAL_SERVER_ERROR, ["error" => "Usuário não encontrado."]);
        }

        if (!app("hash")->check($request["password"], $user->password)) {
            return $this->respond(Response::HTTP_INTERNAL_SERVER_ERROR, ["error" => "Senha incorreta."]);
        }

        $token = $this->generateToken($user);

        if ($user->isAdmin()) {
            return $this->respond("200", ["usuario" => $user, "token" => $token]);
        } else {
            return $this->respond("401", ["status" => "Forbidden. Not an ADMIN."]);
        }
    }

    public function loginProvider(Request $request, $provider)
    {
        $authProvider = $this::TIPOAUTH;

        if (!$request["email"]) {
            return $this->respond(Response::HTTP_INTERNAL_SERVER_ERROR, ["error" => "Email não informado."]);
        }

        $tipoAuthId = $authProvider::where("descricao", $provider)->first();

        $user = Usuario::where("email", $request["email"])->with("imagens", "telefones", "republica")->first();

        if (!$user) {
            return $this->respond(Response::HTTP_NOT_FOUND, ["status" => "Usuário não existe."]);
        } else {
            if ($user->tipoAuth_id !== $tipoAuthId->id) {
                return $this->respond(Response::HTTP_CONFLICT, ["status" => "Esse email já está cadastrado por outro provedor."]);
            }

            if (!app("hash")->check($request["password"], $user->password)) {
                return $this->respond(Response::HTTP_INTERNAL_SERVER_ERROR, ["status" => "Senha está incorreta."]);
            }
        }

        $user->notificationToken = $request["notificationToken"];
        
        $user->refreshTokenId = $this->generateRefreshTokenId();

        $user->save();

        $token = $this->generateToken($user);

        return $this->respond(Response::HTTP_OK, ["user" => $user, "token" => $token, "refreshTokenId" => $user->refreshTokenId ]);
    }

    public function registerWithProvider(Request $request, $provider)
    {
        $model = $this::MODEL;
        $tipoauth = $this::TIPOAUTH;
        $t = $this::TEL;

        $provider = $tipoauth::where("descricao", "=", $provider)->first();

        $this->validate($request, $model::$rules);

        $request["password"] = app("hash")->make($request["password"]);

        $request["tipoAuth_id"] = $provider->id;

        $telefones = $request["telefones"];

        $user = $model::create($request->all());

        $user->imagens()->create([
            "url" => $request["imagem"]
        ]);

        $user->notificationToken = $request['notificationToken'];

        $user->refreshTokenId = $this->generateRefreshTokenId();

        $user->save();
        
        if (!empty($telefones["numeroTelefone"])) {
            //Salva os telefones do usuario 1 ou muitos
            $user->telefones()
            ->save($t::create([
                "numeroTelefone" => $telefones["numeroTelefone"],
                "tipoTelefone_id" => $telefones["idTipoTelefone"]
            ]));
        }

        $token = $this->generateToken($user);

        $user = $model::where("id", $user->id)->with("imagens", "telefones", "republica")->first();

        return $this->respond(Response::HTTP_CREATED, ["usuario" => $user, "token" => $token, "refreshTokenId" => $user->refreshTokenId ]);
    }

    public function logout()
    {
    }
    
    public function removeUser(Request $request, $id)
    {
        //Verificação
        if (!Auth::check()) {
            return $this->respond(Response::HTTP_UNAUTHORIZED, ["status" => "Unauthorized"]);
        }
        $user = Auth::User();
        if ($user->id !== $id) {
            return $this->respond(Response::HTTP_UNAUTHORIZED, ["status" => "Unauhorized"]);
        }
        //Exclusão
        if (is_null(Usuario::find($id))) {
            return $this->respond(Response::HTTP_NOT_FOUND);
        }

        if ($user->tipoConta === "Searcher") {
            $user->imagens()->delete();
            $user->telefones()->delete();
            $user->vagas()->detach();
            $user->visitas()->delete();
        }
        if ($user->tipoConta === "Announcer") {
            $user->imagens()->delete();
            $user->telefones()->delete();
            $user->republicas()->get()->each(function ($republica, $key) {
                $republica->vagas()->get()->each(function ($vaga, $key) {
                    $vaga->usuarios()->detach();
                    $vaga->visitas()->delete();
                });
                $republica->vagas()->delete();
                $republica->imagens()->delete();
                $republica->telefones()->delete();
                $republica->conveniencias()->detach();
            });
            $user->republicas()->delete();
        }


        Usuario::destroy($id);
        return $this->respond(Response::HTTP_OK, ["status" => "Removido com sucesso."]);
    }

    public function sairRepublica($id) {
        if (!Auth::Check()) {
            return $this->respond(Response::HTTP_UNAUTHORIZED, ["status" => "Unauthorized."]);
        }
        $usuario = Auth::User();

        if ($usuario->tipoConta != "Searcher") {
            // return $this->respond(Response::HTTP_UNAUTHORIZED, ["status" => "Não permitido para anunciadores."]);
        }

        $republica = Republica::find($id);

        if(is_null($republica)) {
            return $this->respond(Response::HTTP_NOT_FOUND, ["status" => "República não encontrada."]);
        }

        $usuario->republica()->dissociate($republica);
        
        $usuario->save();

        return $this->respond(Response::HTTP_NO_CONTENT);
    }

    public function updateUsuarioComSenha(Request $request, $id)
    {

        $m = $this::MODEL;

        //Hash da senha
        $request["password"] = app("hash")->make($request["password"]);

        $new_tel = $request["contato"];

        $user = $m::find($id);

        if (!empty($new_tel)) {
            $telefone = $user->telefones()->first();
            $tipo = TipoTelefone::where("descricao", "celular")->first();
            if (empty($telefone)) {
                $user->telefones()->save(Telefone::create([
                    "numeroTelefone" => $new_tel,
                    "tipoTelefone_id" => $tipo->id
                ]));
            } else {
                $telefone->numeroTelefone = $new_tel;
                
                $telefone->save();
            }
        }

        if (is_null($user)) {
            return $this->respond("500", ["status" => "Usuário não encontrado."]);
        }

        $user->update($request->all());

        return $this->respond("200", ["status" => "Alterado com sucesso."]);
    }

    public function updateUsuario(Request $request, $id)
    {

        $m = $this::MODEL;

        $new_tel = $request["contato"];

        $user = $m::find($id);

        if (is_null($user)) {
            return $this->respond("500", ["status" => "Usuário não encontrado."]);
        }

        if (!empty($new_tel)) {
            $telefone = $user->telefones()->first();
            $tipo = TipoTelefone::where("descricao", "celular")->first();
            if (empty($telefone)) {
                $user->telefones()->save(Telefone::create([
                    "numeroTelefone" => $new_tel,
                    "tipoTelefone_id" => $tipo->id
                ]));
            } else {
                $telefone->numeroTelefone = $new_tel;
                
                $telefone->save();
            }
        }

        $user->update($request->all());

        return $this->respond("200", ["status" => "Alterado com sucesso."]);
    }

    public function updateUserImage(Request $request)
    {
        $url = $request["url"];

        if (!isset($url)) {
            return $this->respond(Response::HTTP_UNPROCESSABLE_ENTITY, ["status" => "Informe uma URL para salvar."]);
        }

        $user = Auth::User();

        $imagem = $user->imagens()->first();

        if (empty($imagem)) {
            $user->imagens()->save(Imagem::Create([
                "url" => $url
            ]));

            $msg = "Criado com sucesso.";
        } else {
            $imagem->url = $url;
            
            $msg = "Alterado com sucesso.";
            
            $imagem->save();
        }
        
        
        return $this->respond(Response::HTTP_OK, ["status" => $msg]);
    }

    public function applyToVacant(Request $request, $id)
    {
        $vaga = $this::VAGA;

        $user = Auth::User();

        $vaga = $vaga::find($id);
        
        if (\is_null($vaga)) {
            return $this->respond(Response::HTTP_BAD_REQUEST, ["status" => "Vaga inexistente"]);
        }

        $notificationToken = $vaga::find($id)->republica()->first()->usuario()->first()->notificationToken;

        $notification = $this->sendMessage($notificationToken, $user, null, Notification::NOTIFICATION_APPLIED);

        $notified = false;

        if ($notification->errors) {
            $user->vagas()->attach($id, ["notificationId" => null, "situacao" => "Não Notificado"]);
        } else {
            $user->vagas()->attach($id, ["notificationId" => $notification->id, "situacao" => "Notificado"]);
            $notified = true;
        }
        return $this->respond("201", [
        "status" => [
            "notified" => $notified,
            "message" => "Candidatado a vaga com sucesso."
            ]
        ]);
    }

    public function unapplyToVacant(Request $request, $id)
    {
        $user = Auth::User();
        
        $notificationId = $user->vagas()->find($id)->notificationId;

        $response = $this->cancelMessage($notificationId);

        $user_vaga = $user->vagas()->updateExistingPivot($id, ["situacao" => "Cancelada"]);

        return $this->respond("200", ["status" => "Interesse na vaga removido, poderá reativar quando quiser."]);
    }

    public function reApplyToVacant($id)
    {
        $user = Auth::User();

        $notificationToken = Vaga::find($id)->republica()->first()->usuario()->first()->notificationToken;

        $notification = $this->sendMessage($notificationToken, $user, null, Notification::NOTIFICATION_APPLIED);
        
        $notified = false;

        if ($notification->errors) {
            $user->vagas()->updateExistingPivot($id, ["situacao" => "Não Notificado"]);
        } else {
            $user->vagas()->updateExistingPivot($id, ["situacao" => "Notificado"]);
            $notified = true;
        }

        return $this->respond(Response::HTTP_OK, ["status" => "Interesse aplicado."]);
    }

    public function getAllVacantsApplied()
    {
        $user = Auth::User();

        if (empty($user)) {
            return $this->respond(Response::HTTP_UNAUTHORIZED, ["status" => "Usuário do token inválido."]);
        }

        $vacants = $user->vagas()->with("republica", "republica.usuario")->get()->groupBy("pivot.situacao");


        return $this->respond(Response::HTTP_OK, $vacants);
    }

    public function updatePushToken(Request $request)
    {
        $pushToken = $request["notificationToken"];

        $user = Auth::User();

        $user->notificationToken = $pushToken;

        $user->save();

        return $this->respond(Response::HTTP_ACCEPTED, ["status" => "Updated with success."]);
    }

    public function updateFirebaseUid(Request $request)
    {
        $user = Usuario::where("id", $request["user_id"])->first();

        if (!$user) {
            return $this->respond(Response::HTTP_BAD_REQUEST, ["status" => "Usuário não existente."]);
        }

        if (!isset($request["firebase_uid"])) {
            return $this->respond(Response::HTTP_UNPROCESSABLE_ENTITY, ["status" => "É necessario informar o UID do Firebase."]);
        }

        $user->firebaseUid = $request["firebase_uid"];

        $user->save();

        return $this->respond(Response::HTTP_OK, ["status" => "UID salvo."]);
    }

    private function generateToken($user)
    {

        $now = time();

        $payload = array(
            "alg" => "RS256",
            "iss" => "firebase-adminsdk-ryclw@haus-4a39b.iam.gserviceaccount.com",
            "aud" => "https://identitytoolkit.googleapis.com/google.identity.identitytoolkit.v1.IdentityToolkit",
            "iat" => $now,
            "exp" => $now + (60 * 60),
            "sub" => "firebase-adminsdk-ryclw@haus-4a39b.iam.gserviceaccount.com",
            "uid" => $user->id,
            "claims" => array(
                "email" => $user->email
            )
        );

        $token = JWT::encode($payload, $this::PRIVATE_KEY, "RS256", "15334614c221761437dc59c0321a2aa4aa5bc230");
        return $token;
    }

    public function refreshToken(Request $request)
    {
        
        if (!isset($request["refresh_token_id"])) {
            return $this->respond(Response::HTTP_UNAUTHORIZED, ["error" => "Unauthorized. Refresh token id must be given"]);
        }
        if (!isset($request["api_secret"])) {
            return $this->respond(Response::HTTP_UNAUTHORIZED, ["error" => "Unauthorized. Api secret must be given"]);
        }

        $m = $this::MODEL;

        $user = $m::where("refreshTokenId", "=", $request["refresh_token_id"])->first();

        if (is_null($user)) {
            return $this->respond(Response::HTTP_UNAUTHORIZED, ["error" => "Unauthorized"]);
        }

        $api_secret = $request["api_secret"];

        if ($api_secret === env("API_SECRET")) {
            try {
                $user->refreshTokenId = $this->generateRefreshTokenId();

                $token = $this->generateToken($user);

                $user->save();
                
                return $this->respond(Response::HTTP_ACCEPTED, ["token" => $token, "refreshTokenId" => $user->refreshTokenId]);
            } catch (Exception $e) {
                var_dump($e);
            }
        } else {
            return $this->respond(Response::HTTP_UNAUTHORIZED, ["error" => "Unauthorized. wrong api secret"]);
        }
    }

    public function validateToken(Request $request)
    {
        $token = $request->query("token");
        $valid;
        try {
            $decoded = JWT::decode($request["token"], $this::PUBLIC_KEY, array("RS256"));
            $valid = true;
        } catch (\Firebase\JWT\ExpiredException $e) {
            $valid = false;
        } catch (\Exception $e) {
            dd($e);
            $valid = false;
        }
        return $this->respond(Response::HTTP_OK, ["status" => $valid]);
    }
    private function generateRefreshTokenId()
    {
        return substr(md5(openssl_random_pseudo_bytes(20)), -32);
    }
}
