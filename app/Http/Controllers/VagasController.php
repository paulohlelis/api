<?php namespace App\Http\Controllers;

use Event;
use App\Events\VagaCriada;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Carbon\Carbon;
use App\Vaga;
use Auth;


class VagasController extends Controller {

    const MODEL = "App\Vaga";
    const REPUBLICA = "App\Republica";

    use RESTActions;

    public function deleteVaga($id) {

        if(is_null(Vaga::find($id))){
            return $this->respond(Response::HTTP_NOT_FOUND);
        }
        Vaga::find($id)->usuarios()->detach();

        Vaga::find($id)->visitas()->delete();

        Vaga::destroy($id);

        return $this->respond(Response::HTTP_OK, ['status' => 'Removido com sucesso.']);
    }

    // OK
    public function cadastrarVaga(Request $request) {

        if(Auth::User()->republicas()->get()->count() <= 0){
           return $this->respond('500', ['status' => 'Nenhuma república cadastrada.']);
        }
        
        $this->validate($request, Vaga::$rules);

        $vaga = Vaga::create($request->all());

        $despesas = isset($request['despesas']) ? $request['despesas'] : array();
        
        //Insere Despesas na Vaga
        if (!empty($despesas)) {
            foreach ($despesas as $key => $despesa) {
                $vaga->despesas()
                    ->attach($despesa);
            }
        }

        $vaga = Vaga::where('id', $vaga->id)->with('republica', 'usuarios', 'despesas', 'usuarios.imagens', 'republica.imagens', 'republica.conveniencias', 'republica.telefones', 'republica.universidade', 'republica.usuario')->first();

        Event::fire(new VagaCriada($vaga));

        
       return $this->respond(Response::HTTP_CREATED, $vaga);
    }

    // OK
    public function buscarVagas(Request $request) {

        $m = $this::MODEL;

        $tipoRepublica = $request->input('tipo');
        $universidades = $request->input('universidades');
        
        if(empty($universidades) && empty($tipoRepublica)) {
            $vagas = $m::with('republica', 'despesas', 'republica.usuario', 'republica.imagens', 'republica.conveniencias', 'republica.telefones')->get();

            return $this->respond(Response::HTTP_OK, $vagas);
        };

        if (!empty($universidades)) {
            $universidades = explode(',', $universidades);
        }

        if ($tipoRepublica) {
            if ($tipoRepublica != 'Masculina' && $tipoRepublica != 'Feminina' && $tipoRepublica != 'Mista') {
                return $this->respond('500', ['error' => 'Busque um tipo existente.']);
            }
        }

        // University & Type
        if (!empty($universidades) && !empty($tipoRepublica)) {
            $resultado = $m::whereHas('republica', function($query) use ($universidades, $tipoRepublica) {
                $query->where('tipoRepublica', '=', $tipoRepublica)
                ->whereIn('universidade_id', $universidades);
            })->with('republica', 'despesas', 'republica.usuario', 'republica.imagens', 'republica.conveniencias', 'republica.telefones')->get();
        }

        // Type
        elseif (!empty($tipoRepublica)) {
            $resultado = $m::whereHas('republica', function ($query) use ($tipoRepublica) {
                $query->where('tipoRepublica', '=', $tipoRepublica);
            })->with('republica', 'despesas', 'republica.usuario', 'republica.imagens', 'republica.conveniencias', 'republica.telefones')->get();
        }

        // University
        elseif (!empty($universidades)) {
            $resultado = $m::whereHas('republica', function ($query) use ($universidades) {
                $query->whereIn('universidade_id', $universidades);
            })->witch('republica', 'despesas', 'republica.usuario', 'republica.imagens', 'republica.conveniencias', 'republica.telefones')->get();
        } else {
            $resultado = '';
        }

        return $this->respond('200', $resultado);

    }

    public function receivedNotification($id)
    {
        $vaga = $this::MODEL;

        $vaga->usuarios()->find($id)->updateExistingPivot(['situacao' => 'Recebido']);
        
        return $this->respond(Response::HTTP_NO_CONTENT);
    }

    public function getCandidatos ($id) {
        if(!isset($id)) {
            return $this->respond(Response::HTTP_UNPROCESSABLE_ENTITY, ['status' => 'É preciso informar o id da vaga']);
        }

        $vaga = Vaga::where('id', $id)->first();

        if(is_null($vaga)) {
            return $this->respond(Response::HTTP_NOT_FOUND, ['status' => 'Vaga inexistente']);
        }

        $candidatos = $vaga->usuarios()->with('imagens')->get();

        return $this->respond(Response::HTTP_OK, $candidatos);
    }
    
    // OK
    public function userIsApplied($user_id, $vaga_id) {
        $model = $this::MODEL;

        $vaga = $model::find($vaga_id);

        $isApplied = $vaga->usuarios()->where('usuario_id', '=', $user_id)->get()->isNotEmpty();

        return $this->respond(Response::HTTP_OK, ['status' => $isApplied]);
    }

    // OK
    public function changeState($id) {
        if (empty($id)) {
            return $this->respond(Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $vaga = Vaga::find($id);

        if (empty($vaga)) {
            return $this->respond(Response::HTTP_NOT_FOUND);
        }

        $active = $vaga->isActive;

        $vaga->isActive = !$active;

        $vaga->save();

       return $this->respond(Response::HTTP_OK, ['status' => $active ? 'Vaga desativada.' : 'Vaga ativada.']);
    }

    // OK
    public function getCountApplies ($id) {
        $vagas = $this::MODEL;
        $count = $vagas::find($id)->usuarios()->count();
        
        return $this->respond(Response::HTTP_OK, $count);
    }
    
}