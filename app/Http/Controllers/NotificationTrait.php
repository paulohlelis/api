<?php namespace App\Http\Controllers;

use App\Usuario;
use App\Vaga;
use App\Visita;
use App\Consts\Notification;

trait NotificationTrait
{

    function sendMessage($notificationToken, $user, $visit, $type)
    {
        if ($type == Notification::NOTIFICATION_APPLIED) {
            $content = array("en" => 'Alguém se interessou na sua vaga. Clique e marque uma visita.');
        } elseif ($type == Notification::NOTIFICATION_APPLY_IS_NEAR) {
            if($user) {
                $content = array("en" =>  $user->nome.' tem uma visita marcada na sua república hoje ás '.$visit->data->format('H:i').'.');
            } else {
                $content = array("en" =>  'Você tem uma visita marcada hoje ás '.$visit->data->format('H:i').' na república '. $visit->vaga()->first()->republica()->first()->nomeRepublica .'.');
            }
        }
        
        $fields = array(
            'app_id' => env('ONESIGNAL_APP_ID'),
            'include_player_ids' => array($notificationToken),
            'data' => array("userId" => $user ? $user->id : '', "visitId" => $visit ? $visit->id : ''),
            'contents' => $content
        );
        $fields = json_encode($fields);        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic '.env('ONESIGNAL_AUTH_KEY')));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        $response = json_decode($response);
        curl_close($ch);

        return $response;
    }

    public function cancelMessage($notificationId)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications/".$notificationId."?app_id=".env('ONESIGNAL_APP_ID'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Basic '.env('ONESIGNAL_AUTH_KEY')));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $result = json_decode($result);
        curl_close($ch);
        return $result;
    }
}
