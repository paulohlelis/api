<?php namespace App\Http\Controllers;

use Event;
use App\Events\MoradorAdicionado;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Usuario;
use App\Republica;
use App\Endereco;
use App\Imagem;
use App\Telefone;

class RepublicasController extends Controller
{

    const MODEL = "App\Republica";
    const IMG = "App\Imagem";
    const TEL = "App\Telefone";

    use RESTActions, ImagemsTrait, EnderecoTrait;

    public function getAll() {

        $republicas = Republica::all()->transform(function ($republica) {
            return $republica->getMapsInfo();
        });
        
        return $republicas;
        
    }

    public function cadastrarRepublica(Request $request) {

        if (!$request['endereco']) {
            return $this->respond('500', ['status' => 'Favor digitar o endereço.']);
        }

        //pegar o endereco
        $endereco = json_decode(json_encode($request['endereco']), null);

        //checa se o estado ja existe no banco
        $estado = $this->checaEstado($endereco->estado);

        //checa se a cidade ja existe no banco
        $cidade = $this->checaCidade($endereco->cidade, $estado);

        //checa se o bairro ja existe no banco
        $bairro = $this->checaBairro($endereco->bairro, $cidade);

        /*$endereco['estado_id'] = $estado->id;
        $endereco['cidade_id'] = $cidade->id;*/
        $endereco->bairro_id = $bairro->id;

        //cria o novo endereco
        $endereco = Endereco::create(get_object_vars($endereco));

        //Inclui o id do Endereço criado na request feita
        $request['endereco_id'] = $endereco->id;

        $usuario = Auth::User();

        $request['usuario_id'] = $usuario->id;

        $this->validate($request, Republica::$rules);

        $republica = Republica::create($request->all());

        /*
         * Pega os telefones e as Conveniencias
         * Retorna um array vazio caso nao seja recebido nenhum valor.
         */

        $telefones = isset($request['telefones']) ? $request['telefones'] : array();

        //Insere Telefones na Republica
        if (!empty($telefones)) {
            foreach ($telefones as $key => $telefone) {
                $republica->telefones()
                    ->save(Telefone::create([
                    'numeroTelefone' => $telefone['numeroTelefone'],
                    'tipoTelefone_id' => $telefone['tipoTelefone_id']
                    ]));
            }
        }

        $conveniencias = isset($request['conveniencias']) ? $request['conveniencias'] : array();
        
        //Insere Conveniencias na Republica
        if (!empty($conveniencias)) {
            foreach ($conveniencias as $key => $conveniencia) {
                $republica->conveniencias()
                    ->attach($conveniencia);
            }
        }

        if ($request['imagens']) {
            $res = $this->UploadImages($request['imagens'], $republica);
        }

        $republica = Republica::where('id', $republica->id)->with(['endereco',
        'endereco.complemento',
        'endereco.bairro',
        'endereco.bairro.cidade',
        'endereco.bairro.cidade.estado',
        'imagens',
        'universidade',
        'vagas',
        'conveniencias'
        ])->first();

        return $this->respond('201', $republica);
    }

    public function alterarRepublica(Request $request, $id) {
        if (!Auth::check()) {
            return $this->respond(Response::HTTP_UNAUTHORIZED, ["status" => "Unauthorized"]);
        }

        if (!$this->checkUuid($id)) {
            return $this->respond(Response::HTTP_UNPROCESSABLE_ENTITY, ['status' => 'Envie um ID válido.']);
        }

        $this->validate($request, Republica::$rules);

        $model = Republica::find($id);

        if (is_null($model)) {
            return $this->respond(Response::HTTP_NOT_FOUND);
        }

        $model->update($request->all());

        $model = Republica::where('id', $id)->with(['endereco',
        'endereco.complemento',
        'endereco.bairro',
        'endereco.bairro.cidade',
        'endereco.bairro.cidade.estado',
        'imagens',
        'universidade',
        'vagas',
        'conveniencias'
        ])->first();

        return $this->respond(Response::HTTP_OK, $model);
    }

    public function getAllByUser($id) {

        if (!$this->checkUuid($id)) {
            return $this->respond(Response::HTTP_UNPROCESSABLE_ENTITY, ['status' => 'Envie um ID válido.']);
        }

        $republicas = Republica::where('usuario_id', '=', $id)
            ->with(['endereco',
            'endereco.complemento',
            'endereco.bairro',
            'endereco.bairro.cidade',
            'endereco.bairro.cidade.estado',
            'imagens',
            'universidade',
            'vagas',
            'conveniencias'
            ])
        ->get();

        //Não encontrou nada
        if (sizeof($republicas) == 0) {
            return $this->respond(Response::HTTP_NOT_FOUND, ['status' => 'Nothing found for this user']);
        }

        //Retorna as republicas
        return $this->respond(Response::HTTP_OK, $republicas);
    }

    public function addMorador(Request $request) {
        if (!Auth::Check()) {
            return $this->respond(Response::HTTP_UNAUTHORIZED, ["status" => "Precisa estar autenticado."]);
        }
        $user = Auth::User();
        if ($user->tipoConta != "Announcer") {
            return $this->respond(Response::HTTP_UNAUTHORIZED, ["status" => "Precisa ser um dono de república."]);
        }
        $morador = Usuario::find($request["moradorId"]);
        if (is_null($morador)) {
            return $this->respond(Response::HTTP_NOT_FOUND, ["status" => "Morador não existe."]);
        }
        $republica = Republica::find($request["republicaId"]);

        $owner = $user->republicas()->find($republica->id);

        if(is_null($owner))
        {
            return $this->respond(Response::HTTP_UNAUTHORIZED, ["status" => "Você não é proprietário da república."]);
        }

        try {
            $morador->republica()->associate($republica);
            $republica->quantidadeMoradores++;
            $republica->save();
            $morador->save();
            Event::fire(new MoradorAdicionado($republica, $morador->id));
            return $this->respond(Response::HTTP_OK, ["status" => "Morador adicionado à sua república."]);
        }
        catch(Exception $ex) {
            return $this->respond(Response::HTTP_INTERNAL_SERVER_ERROR, ["status" => "Algo deu muito errado."]);
        }
    }

    public function removeMorador(Request $request) {
        if (!Auth::Check()) {
            return $this->respond(Response::HTTP_UNAUTHORIZED, ["status" => "Precisa estar autenticado."]);
        }
        $user = Auth::User();
        if ($user->tipoConta != "Announcer") {
            return $this->respond(Response::HTTP_UNAUTHORIZED, ["status" => "Precisa ser um dono de república."]);
        }
        $morador = Usuario::find($request["moradorId"]);
        if (is_null($morador)) {
            return $this->respond(Response::HTTP_NOT_FOUND, ["status" => "Morador não existe."]);
        }
        $republica = Republica::find($request["republicaId"]);

        $owner = $user->republicas()->find($republica->id);

        if(is_null($owner))
        {
            return $this->respond(Response::HTTP_UNAUTHORIZED, ["status" => "Você não é proprietário da república."]);
        }

        try {
            $user->republica()->dissociate($republica);
            $republica->quantidadeMoradores--;
            $republica->save();
            $user->save();
            return $this->respond(Response::HTTP_OK, ["status" => "Morador removido de sua república."]);
        }
        catch(Exception $ex) {
            return $this->respond(Response::HTTP_INTERNAL_SERVER_ERROR, ["status" => "Algo deu muito errado."]);
        }
    }

    public function getAllMoradores($id) {
        $usuario = Auth::User();
        
        $republica = $usuario->republicas()->find($id);

        if (is_null($republica)) {
            return $this->respond(Response::HTTP_UNAUTHORIZED, ["status" => "Você não é proprietário desta república."]);
        } else {
            $moradores = $republica->moradores()->with('imagens', 'telefones')->get();
            return $this->respond(Response::HTTP_OK, $moradores);
        }
    }

    public function buscarRepublica(Request $request) {

        $tipo = $request->input('tipo');
        $universidades = $request->input('universidades');
        $filter = $request->input('filter');

        if (!empty($universidades)) {
            $universidades = explode(',', $universidades);
        }

        if (!empty($filter)) {
            $filter = explode(',', $filter);
        }

        if ($tipo) {
            if ($tipo <= 0) {
                return $this->respond('500', ['error' => 'Busque um tipo existente.']);
            }
        }

        // University & Type & Filter
        if (!empty($universidades) && !empty($tipo) && !empty($filter)) {
            $resultado = Republica::whereHas('conveniencias', function ($q) use ($filter) {
                $q->whereIn('conveniencia_id', $filter);
            })
                ->where('tipo_republica', '=', $tipo)
                ->whereIn('universidade_id', $universidades)
                ->get();
        }
        
        // University & Type
        if (!empty($universidades) && !empty($tipo) && empty($filter)) {
            $resultado = Republica::where('tipo_republica', '=', $tipo)->whereIn('universidade_id', $universidades)->get();
        }

        // Type
        if (empty($universidades) && !empty($tipo) && empty($filter)) {
            $resultado = Republica::where('tipo_republica', '=', $tipo)->get();
        }

        // Type & Filter
        if (empty($universidades) && !empty($filter) && !empty($tipo)) {
            $resultado = Republica::whereHas('conveniencias', function ($q) use ($filter) {
                $q->whereIn('conveniencia_id', $filter);
            })->where('tipo_republica', '=', $tipo)->get();
        }

        // University
        if (empty($filter) && empty($tipo) && !empty($universidades)) {
            $resultado = Republica::whereIn('universidade_id', $universidades)->get();
        }

        // University & Filter
        if (!empty($filter) && empty($tipo) && !empty($universidades)) {
            $resultado = Republica::whereHas('conveniencias', function ($q) use ($filter) {
                $q->whereIn('conveniencia_id', $filter);
            })->whereIn('universidade_id', $universidades)->get();
        }

        // Filter
        if (!empty($filter) && empty($universidades) && empty($tipo)) {
            $resultado = Republica::whereHas('conveniencias', function ($q) use ($filter) {
                $q->whereIn('conveniencia_id', $filter);
            })->get();
        }

        if (!count($resultado)) {
            return $this->respond('204', '');
        }

        return $this->respond('200', $resultado);
    }

    public function getAllVagas($id) {
        if (!$this->checkUuid($id)) {
            return $this->respond(Response::HTTP_UNPROCESSABLE_ENTITY, ['status' => 'Envie um ID válido.']);
        }

        $republica = Republica::where('id', $id)->first();

        if (\is_null($republica)) {
            return $this->respond(Response::HTTP_UNPROCESSABLE_ENTITY, ['status' => 'Esta república não existe.']);
        }

        $vagas = $republica->vagas()->with('republica', 'despesas', 'usuarios', 'usuarios.imagens', 'republica.imagens', 'republica.conveniencias', 'republica.telefones', 'republica.universidade', 'republica.usuario')->get()->groupBy('isActive');

        return $this->respond(Response::HTTP_OK, $vagas);
    }

    public function getVagasCandidatos($id) {
        if (!$this->checkUuid($id)) {
            return $this->respond(Response::HTTP_UNPROCESSABLE_ENTITY, ['status' => 'Envie um ID válido.']);
        }

        $republicas = Republica::where('usuario_id', '=', $id)
        ->with([
            'usuario',
            'vagas',
            'vagas.usuarios'
        ])->get();

        //Não encontrou nada
        if (sizeof($republicas) == 0) {
            return $this->respond(Response::HTTP_NOT_FOUND, ['status' => 'Nothing found for this user']);
        }

        //Retorna as republicas
        return $this->respond(Response::HTTP_OK, $republicas);
    }

    public function removerRepublica($id) {
        //Verificação
        if (!Auth::check()) {
            return $this->respond(Response::HTTP_UNAUTHORIZED, ["status" => "Unauthorized"]);
        }

        if (!$this->checkUuid($id)) {
            return $this->respond(Response::HTTP_UNPROCESSABLE_ENTITY, ['status' => 'Envie um ID válido.']);
        }

        //Exclusão
        if (is_null(Republica::find($id))) {
            return $this->respond(Response::HTTP_NOT_FOUND);
        }

        Republica::find($id)->conveniencias()->detach();

        Republica::find($id)->imagens()->delete();

        Republica::find($id)->vagas()->each(function($vaga) {
            $vaga->despesas()->detach();
            $vaga->usuarios()->detach();
        });
        Republica::find($id)->vagas()->delete();

        Republica::destroy($id);
        
        return $this->respond(Response::HTTP_OK, 'Removido com sucesso.');
    }

    private function checkUuid($uuid) {
        return preg_match('/^[0-9a-f]{8}-([0-9a-f]{4}-){3}[0-9a-f]{12}$/', $uuid);
    }
}
