<?php namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use App\Vaga;
use App\Visita;
use App\Republica;
use App\Usuario;
use App\Consts\Notification;

class VisitasController extends Controller
{

    const MODEL = "App\Visita";

    use RESTActions, NotificationTrait;


    // Agendar uma nova visita.
    public function agendarVisita(Request $request)
    {
        $dataVisita = $request['data_visita'];
        $usuarioId = $request['usuario_id'];
        $vagaId = $request['vaga_id'];

        // Verifica se todos os parâmetros foram informados.
        if (empty($usuarioId) || empty($vagaId || empty($dataVisita))) {
            return $this->respond(Response::HTTP_UNPROCESSABLE_ENTITY, ['status' => 'Faltam parâmetros.']);
        }

        $vaga = Vaga::find($vagaId);

        // Verifica se quem está fazendo a requisição é o dono da república se não for retorna um erro não autorizado
        if ($vaga->republica()->first()->usuario_id !== Auth::User()->id) {
            return $this->respond(Response::HTTP_UNAUTHORIZED, ['Unauthorized']);
        }

        $usuario = Visita::where('usuario_id', $usuarioId)->where('finalizada', false)->first();

        // Verifica se o candidato já possui uma visita marcada que não foir encerrada ainda
        if ($usuario) {
            return $this->respond(Response::HTTP_CONFLICT, ['status' => 'Já existe uma visita programada para este usuário.']);
        }

        // Transforma a data para o padrão do Carbon
        $dataVisita = Carbon::parse($dataVisita);

        // Finalmente cria a visita no dia e hora informados
        $visita = Visita::create([
            'data' => $dataVisita,
            'usuario_id' => $usuarioId,
            'vaga_id' => $vagaId,
            'notificationSent' => false
        ]);

        return $this->respond(Response::HTTP_CREATED, $visita);
    }

    // Verifica as próximas visitas
    public function checkNextVisits()
    {
        $model = $this::MODEL;

        $nextVisits = $model::where('data', '>=', Carbon::now()->subHour())->get();
        $pastedVisits = $model::where('data', '<', Carbon::yesterday())->get();
        try {
            $nextVisits->each(function ($visit, $key) {
                if (!$visit->notificationSent) {
                    $user = $visit->usuario()->first();
                    $vagaOwnerNotificationToken = $visit->vaga()->first()->republica()->first()->usuario()->first()->notificationToken;

                    $userStatus = $this->sendMessage($vagaOwnerNotificationToken, $user, $visit, Notification::NOTIFICATION_APPLY_IS_NEAR);
                    $ownerStatus = $this->sendMessage($user->notificationToken, null, $visit, Notification::NOTIFICATION_APPLY_IS_NEAR);


                    if (!$userStatus->errors && !$ownerStatus->errors) {
                        $visit->notificationSent = true;
                        $visit->save();
                    }
                }
            });
            $pastedVisits->each(function ($visit, $key) {
                $visit->finalizada =  true;
                $visit->save();
            });

            return $this->respond(Response::HTTP_OK, ['message' => 'Successfully sent message.']);
        } catch (Exception $error) {
            return $this->respond(Response::HTTP_INTERNAL_SERVER_ERROR, ['message' => 'Nothing was sent.']);
        }
    }

    // Busca todas as visitas de uma determinada republcia
    public function getAllRepublicVisits($id)
    {
        $visitas = $this::MODEL;

        $allVisits = $visitas::all()->transform(function ($visita, $key) use ($id) {
            $vaga = $visita->vaga()->first();
            $usuario = $visita->usuario()->with('imagens', 'republica')->first();
            $republica = $vaga->republica()->first();
            if ($vaga->republica_id == $id && !$visita->finalizada) {
                $vaga['republica'] = $republica->nomeRepublica;
                $visita['vaga'] = $vaga; 
                $visita['usuario'] = $usuario;
                return $visita;
            }
        })->toArray();

        return $this->respond(Response::HTTP_OK, $allVisits);
    }

    // Atualiza a uma visita para visualizada
    public function atualizarVisualizacao($id)
    {
        $visita = Visita::find($id);

        if (empty($visita)) {
            return $this->respons(Response::HTTP_NOT_FOUND, ['status' => 'Visita não existe.']);
        }

        $visita->visualizada =  true;

        $visita->save();

        return $this->respond(Response::HTTP_NO_CONTENT);
    }

    // Atualiza uma visita para finalizada
    public function finalizarVisita($id)
    {
        $visita = Visita::find($id);
        
        if (empty($visita)) {
            return $this->respons(Response::HTTP_NOT_FOUND, ['status' => 'Visita não existe.']);
        }
        
        $visita->finalizada =  true;
        
        $visita->save();
        
        return $this->respond(Response::HTTP_NO_CONTENT);
    }
}
