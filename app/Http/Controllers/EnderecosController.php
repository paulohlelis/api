<?php namespace App\Http\Controllers;

use Illuminate\Http\Response;
use App\Endereco;

class EnderecosController extends Controller {

    const MODEL = "App\Endereco";

    use RESTActions;

    public function getEndereco($endereco_id) {
        if(!\is_numeric($endereco_id)){
            return $this->respond(Response::HTTP_UNPROCESSABLE_ENTITY, ['status' => 'ID inválido.']);
        }

        $endereco = Endereco::find($endereco_id)->first();

        if(is_null($endereco)) {
            return $this->respond(Response::HTTP_NOT_FOUND, ['status' => 'Endereco inxistente.']);
        }

        return $this->respond(Response::HTTP_OK, [
            'cep' => $endereco->cep,
            'logradouro' => $endereco->logradouro,
            'numeroApto' => $endereco->numeroApto,
            'numero' => $endereco->numero, 
            'complemento_id' => $endereco->complemento_id,
            'bairro' => $endereco->bairro()->first()->nomeBairro,
            'cidade' => $endereco->bairro()->first()->cidade()->first()->nomeCidade,
            'estado' => $endereco->bairro()->first()->cidade()->first()->estado()->first()->nomeEstado]);
    }

}
