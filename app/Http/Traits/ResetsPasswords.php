<?php
namespace App\Http\Traits;

use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Hashing\BcryptHasher;
use Illuminate\Support\Facades\Password;

trait ResetsPasswords
{
    
    public function postEmail(Request $request)
    {
        return $this->sendResetLinkEmail($request);
    }

    public function sendResetLinkEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);
        $broker = $this->getBroker();
        $response = Password::broker($broker)->sendResetLink($request->only('email'), function (Message $message) {
            $message->subject($this->getEmailSubject());
        });
        switch ($response) {
            case Password::RESET_LINK_SENT:
                return $this->getSendResetLinkEmailSuccessResponse($response);
            case Password::INVALID_USER:
            default:
                return $this->getSendResetLinkEmailFailureResponse($response);
        }
    }
    protected function getEmailSubject()
    {
        return property_exists($this, 'subject') ? $this->subject : 'Link para alterar a senha';
    }
    protected function getSendResetLinkEmailSuccessResponse($response)
    {
        return response()->json(['success' => true]);
    }
    protected function getSendResetLinkEmailFailureResponse($response)
    {
        return response()->json(['success' => false]);
    }
    public function postReset(Request $request)
    {
        return $this->reset($request);
    }
    
    public function reset(Request $request)
    {
        $this->validate($request, $this->getResetValidationRules());

        $credentials = $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );

        $broker = $this->getBroker();

        $response = Password::broker($broker)->reset($credentials, function ($user, $password) {
            $this->resetPassword($user, $password);
        });

        switch ($response) {
            case Password::PASSWORD_RESET:
                return $this->getResetSuccessResponse($response);
            case Password::INVALID_TOKEN:
                return response()->json(['status' => 'Token inválido ou vencido.']);
            default:
                return $this->getResetFailureResponse($request, $response);
        }
    }

    protected function getResetValidationRules()
    {
        return [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ];
    }

    protected function resetPassword($user, $password)
    {
        $user->password = app('hash')->make($password);
        $user->save();
        
        return response()->json(['success' => true]);
    }

    protected function getResetSuccessResponse($response)
    {
        return response()->json(['success' => true]);
    }

    protected function getResetFailureResponse(Request $request, $response)
    {
        return response()->json(['success' => false]);
    }

    public function getBroker()
    {
        return property_exists($this, 'broker') ? $this->broker : null;
    }
}
