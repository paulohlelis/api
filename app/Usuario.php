<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use App\helpers\ResetPassword as ResetPasswordNotification;
use Illuminate\Notifications\Notifiable;
use App\Http\Traits\UuidsTrait;

class Usuario extends Model implements CanResetPasswordContract {

    use CanResetPassword, Notifiable, UuidsTrait;

    protected $fillable = [
        "nome",
        "sobrenome",
        "email",
        "password",
        "notificationToken",
        "firebaseUid",
        "genero",
        "tipoConta",
        "tipoAuth_id",
        "email_verified"
    ];

    protected $dates = [];

    protected $hidden = ['password', 'refreshTokenId', 'tipoAuth_id'];

    public static $messages = [
        "nome.required" => "O nome precisa ser informado.",
        "sobrenome.required" => "O sobrenome precisa ser informado.",
        "password.required" => "A senha precisa ser informada.",
        "genero.required" => "Você precisa informar seu gênero",
        "email.required" => "Informe um email.",
        "email.unique" => "Já existe um conta com o email informado."
    ];

    public static $rules = [
        "nome" => "required",
        "sobrenome" => "required",
        "password" => "required | same:repeatPass",
        "notificationToken" => "required",
        "email" => "required | unique:usuarios",
        "genero" => "required",
        "tipoConta" => "required"
    ];

    public $incrementing = false;

    // Relationships

    public function imagens() {
        return $this->hasMany("App\Imagem");
    }

    public function tipoAuth() {
        return $this->belongsTo("App\TipoAuth");
    }

    public function telefones(){
        return $this->hasMany("App\Telefone");
    }

    // Announcer (Republicas que ele possui)*
    public function republicas() {
        return $this->hasMany("App\Republica");
    }

    public function vagas() {
        return $this->belongsToMany("App\Vaga")->withPivot('notificationId', 'situacao')->withTimestamps();
    }

    public function visitas() {
        return $this->hasMany("App\Visita");
    }

    // Searcher (republica que ele mora)
    public function republica() {
        return $this->belongsTo("App\Republica");
    }

    public function genero() {
        return $this->belongsTo("App\Genero");
    }

    public function isAdmin() {
        if ( $this->tipoConta == 'Administrador' )
            return true;
        else
            return false;
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }


}
