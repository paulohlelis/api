<?php

namespace App\helpers;

use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPassword extends Notification
{

    public $token;


    public function __construct($token)
    {
        $this->token = $token;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting('Olá, ')
            ->line('Você está recebendo este email por que recebemos um pedido de alteração de senha.')
            ->action('Clique aqui para alterar sua senha através do nosso App.', url("/password/".$this->token))
            ->line('Cado você não tenha pedido a alteração da sua senha, desconsidere este email.')
            ->salutation('Equipe iHaus');
    }
}
