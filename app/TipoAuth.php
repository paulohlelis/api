<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Traits\UuidsTrait;

class TipoAuth extends Model {

    use UuidsTrait;

    protected $fillable = ["descricao"];

    protected $dates = [];

    public static $rules = [
        "descricao" => "required",
    ];

    public $incrementing = false;

    public $timestamps = false;

    public function usuarios()
    {
        return $this->hasMany("App\Usuario");
    }


}
