<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipoAuthTable extends Migration
{

    public function up()
    {
        Schema::create('tipo_auths', function(Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('descricao');
            // Constraints declaration

        });
    }

    public function down()
    {
        Schema::drop('tipo_auths');
    }
}
