<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDespesaVagaTable extends Migration
{

    public function up()
    {
        Schema::create('despesa_vaga', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('despesa_id')->unsigned()->index();
            $table->integer('vaga_id')->unsigned()->index();
            $table->foreign('despesa_id')
                ->references('id')
                ->on('despesas');
            $table->foreign('vaga_id')
                ->references('id')
                ->on('vagas');

        });
    }

    public function down()
    {
        Schema::drop('despesa_vaga');
    }
}
