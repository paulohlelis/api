<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnderecosTable extends Migration
{

    public function up()
    {
        Schema::create('enderecos', function (Blueprint $table) {
            $table->increments('id');
            $table->char('cep', 8);
            $table->string('logradouro', 100);
            $table->smallInteger('numero');
            $table->smallInteger('numeroApto')->nullable();
            $table->float('end_lat', 10, 8);
            $table->float('end_lng', 11, 8);
            $table->integer('bairro_id')->unsigned();
            $table->foreign('bairro_id')
                ->references('id')
                ->on('bairros');
            $table->unsignedSmallInteger('complemento_id');
            $table->foreign('complemento_id')
                ->references('id')
                ->on('complementos');
        });
    }

    public function down()
    {
        Schema::drop('enderecos');
    }
}
