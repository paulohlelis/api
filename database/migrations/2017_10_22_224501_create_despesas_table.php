<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDespesasTable extends Migration
{

    public function up()
    {
        Schema::create('despesas', function(Blueprint $table) {
            $table->increments('id');
            $table->string('descricao');
            // Constraints declaration

        });
    }

    public function down()
    {
        Schema::drop('despesas');
    }
}
