<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagemsTable extends Migration
{

    public function up()
    {
        Schema::create('imagems', function (Blueprint $table) {
            $table->increments('id');
            $table->text('url');
            $table->uuid('republica_id')->nullable();
            $table->uuid('usuario_id')->nullable();

            $table->foreign('usuario_id')
                ->references('id')
                ->on('usuarios');
            
            $table->foreign('republica_id')
                ->references('id')
                ->on('republicas');
        });
    }

    public function down()
    {
        Schema::drop('imagems');
    }
}
