<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuarioVagaTable extends Migration
{

    public function up()
    {
        Schema::create('usuario_vaga', function(Blueprint $table) {
            $table->increments('id');
            $table->uuid('notificationId')->nullable(); // ID gerado ao criar a notificação com o 'OneSignal'. Para ser possível o cancelamento é necessário manter esse ID
            $table->enum('situacao', array('Notificado', 'Não Notificado', 'Cancelada', 'Recebida'));
            $table->uuid('usuario_id')->index();
            $table->integer('vaga_id')->unsigned()->index();
            $table->foreign('usuario_id')
                ->references('id')
                ->on('usuarios');
            $table->foreign('vaga_id')
                ->references('id')
                ->on('vagas');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('usuario_vaga');
    }
}
