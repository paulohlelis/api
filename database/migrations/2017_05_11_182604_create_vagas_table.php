<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVagasTable extends Migration
{

    public function up()
    {
        Schema::create('vagas', function(Blueprint $table) {
            $table->increments('id');
            $table->string('titulo', 100);
            $table->float('valor');
            $table->boolean('isActive')->default(true);
            $table->enum('tipo_de_quarto', ['Individual', 'Compartilhado']);
            $table->boolean('vaga_garagem')->default(false);
            $table->uuid('republica_id');
            $table->foreign('republica_id')
                ->references('id')
                ->on('republicas');
            $table->timestamps();

        });
    }

    public function down()
    {
        Schema::drop('vagas');
    }
}
