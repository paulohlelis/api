<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration
{

    public function up()
    {
        Schema::create('usuarios', function(Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('refreshTokenId', 32);
            $table->string('firebaseUid', 36);
            $table->string('nome', 100);
            $table->string('sobrenome', 100);
            $table->string('email')->unique();
            $table->boolean('email_verified')->default(false);
            $table->string('password');
            $table->uuid('republica_id')->nullable();
            $table->uuid('tipoAuth_id');
            $table->string('notificationToken', 36);
            $table->enum('genero', ['Masculino', 'Feminino']);
            $table->enum('tipoConta', ['Administrador', 'Searcher', 'Announcer'])->default('Searcher');
            $table->foreign('tipoAuth_id')
            ->references('id')
            ->on('tipo_auths');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
