<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitasTable extends Migration
{

    public function up()
    {
        Schema::create('visitas', function (Blueprint $table) {
            $table->increments('id');
            $table->datetime('data');
            $table->boolean('notificationSent')->default(false);
            $table->boolean('visualizada')->default(false);
            $table->boolean('finalizada')->default(false);
            $table->uuid('usuario_id');

            $table->integer('vaga_id')->unsigned();
            $table->foreign('vaga_id')
                ->references('id')
                ->on('vagas');
            $table->foreign('usuario_id')
                ->references('id')
                ->on('usuarios');
        });
    }

    public function down()
    {
        Schema::drop('visitas');
    }
}
