<?php

use Illuminate\Database\Seeder;
use App\TipoAuth;
use App\Usuario;
use App\Complemento;
use App\Conveniencia;
use Illuminate\Hashing\BcryptHasher;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call('TipoAuthSeeder');
        $this->call('UsersSeeder');
        $this->call('ConvenienciaSeeder');
        $this->call('ComplementoSeeder');
        $this->call('TipoTelefoneSeeder');
        $this->call('DespesaSeeder');
    }
}

class TipoAuthSeeder extends Seeder
{
    
    public function run()
    {
        DB::table('tipo_auths')->delete();
            
        TipoAuth::create([
            'descricao' => 'email_password'
        ]);
        TipoAuth::create([
            'descricao' => 'facebook'
        ]);
        TipoAuth::create([
            'descricao' => 'google'
        ]);
    }
}

class UsersSeeder extends Seeder
{
                
    public function run()
    {
        DB::table('usuarios')->delete();

        $tipo = TipoAuth::where('descricao', 'email_password')->first();
                
        Usuario::create([
        'nome' => 'Paulo',
        'sobrenome' => 'Henrique Lelis Maia',
        'email' => 'phlelism@gmail.com',
        'tipoConta' => 'Administrador',
        'genero' => 'Masculino',
        'password' => app('hash')->make('84101048'),
        'tipoAuth_id' => $tipo->id
        ]);
    }
}

class ComplementoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('complementos')->delete();
        
                DB::table('complementos')->insert([
                    ['descricao' => 'Casa'],
                    ['descricao' => 'Apartamento'],
                    ['descricao' => 'Vila'],
                    ['descricao' => 'Beco'],
                    ['descricao' => 'Outro'],
                    ['descricao' => 'Fundos'],
                    ['descricao' => 'Condomínio'],
                    ['descricao' => 'Fazenda']
                ]);
    }
}

class ConvenienciaSeeder extends Seeder
{

    public function run()
    {
        DB::table('conveniencias')->delete();

        DB::table('conveniencias')->insert([
            ['descricao' => 'Campus Próximo'],
            ['descricao' => 'Supermercado Próximo'],
            ['descricao' => 'Ponto de ônibus Próximo'],
            ['descricao' => 'Fármacia Próxima'],
            ['descricao' => 'LGBT'],
            ['descricao' => 'Fumantes'],
            ['descricao' => 'Animais de estimação'],
            ['descricao' => 'Padaria Próxima']
        ]);
    }
}

class TipoTelefoneSeeder extends Seeder
{

    public function run()
    {
        DB::table('tipoTelefones')->delete();

        DB::table('tipoTelefones')->insert([
            ['descricao' => 'Fixo'],
            ['descricao' => 'Celular'],
            ['descricao' => 'Fax']
        ]);
    }
}

class DespesaSeeder extends Seeder
{
    public function run()
    {
        DB::table('despesas')->delete();

        DB::table('despesas')->insert([
            ['descricao' => 'Água'],
            ['descricao' => 'Energia Elétrica'],
            ['descricao' => 'Internet/WiFi'],
            ['descricao' => 'Telefone'],
            ['descricao' => 'Condomínio'],
            ['descricao' => 'TV a Cabo'],
            ['descricao' => 'Diarista']
        ]);
    }
}