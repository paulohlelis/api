<?php

/*
|ROTAS API
*/

$app->group(['prefix' => 'api'], function () use ($app) {

    // Aberta para o Cron poder acessar e executar o script.
    $app->group(['prefix' => 'visita'], function () use ($app) {
        $app->get('verify', 'VisitasController@checkNextVisits');
    });


    $app->group(['prefix' => 'usuario'], function () use ($app) {
        // Cria um usuario
        $app->post('', 'UsuariosController@cadastrar');

        // Cria um usuário com provider
        $app->post('/provider/{provider}', 'UsuariosController@registerWithProvider');

        //Loga um Usuario
        $app->post('login', 'UsuariosController@login');

        // Loga com Provider
        $app->post('/login/{provider}', 'UsuariosController@loginProvider');

        // Desloga o usuario
        $app->get('logout', 'UsuariosController@logout');

        // Renova Token
        $app->post('refreshToken', 'UsuariosController@refreshToken');

        // Verificar email
        $app->get('auth/verifyEmail', 'UsuariosController@verifyEmail');

        //Request new email verification
        $app->get('auth/requestVerification', 'UsuariosController@requestVerificationEmail');

        // Verifica token
        $app->get('verifyToken', 'UsuariosController@validateToken');

        $app->group(['prefix' => 'password'], function () use ($app) {
            // Enviar um email para o usuário alterar sua senha
            $app->post('/email', 'PasswordController@postEmail');
            
            // Altera a senha do usuário se o token for validado
            $app->post('/reset', 'PasswordController@postReset');
        });
    });

    $app->group(['prefix' => 'usuario', 'middleware' => ['auth', 'admin']], function () use ($app) {
         //Retorna Todos Usuarios
         $app->get('', 'UsuariosController@all');
    });

    $app->group(['prefix' => 'usuario', 'middleware'=> 'auth'], function () use ($app) {
        // Retorna Todas as vagas as quais o usuario está candidatado
        $app->get('/vagas', 'UsuariosController@getAllVacantsApplied');

        // Retorna um usuario com id informado
        $app->get('{id}', 'UsuariosController@get');

        // Atualiza o Notification Token do usuario
        $app->put('/pushToken', 'UsuariosController@updatePushToken');

        // Atualiza imagem do usuario
        $app->put('/imagem', 'UsuariosController@updateUserImage');

        //Altera usuario com id informado
        $app->put('{id}', 'UsuariosController@updateUsuario');

        //Retorna os telefones do usuario
        $app->get('{id}/telefones', 'UsuariosController@getComTelefones');

        //Altera usuario e senha com id informado
        $app->put('/password/{id}', 'UsuariosController@updateUsuarioComSenha');

        // Recandidata o usuario a vaga
        $app->put('vaga/{id}', 'UsuariosController@reApplyToVacant');

        //Adiciona o UID do firebase no usuário
        $app->post('/firebase/uid', 'UsuariosController@updateFirebaseUid');

        // Candidata usuario a vaga
        $app->post('vaga/{id}', 'UsuariosController@applyToVacant');

        // Remove um usuario com id informado
        $app->delete('{id}', 'UsuariosController@removeUser');
        
        // Sai da republica que o usuario estiver presente
        $app->delete('{id}/exitRepublica', 'UsuariosController@sairRepublica');

        // Descandidata usuario a vaga
        $app->delete('vaga/{id}', 'UsuariosController@unapplyToVacant');
    });

    $app->group(['prefix'=>'estado', 'middleware'=> 'auth'], function () use ($app) {
        $app->get('', 'EstadosController@all');
        $app->get('{id}', 'EstadosController@get');
        $app->post('', 'EstadosController@add');
        $app->put('{id}', 'EstadosController@put');
        $app->delete('{id}', 'EstadosController@remove');
    });

    $app->group(['prefix'=>'cidade', 'middleware'=> 'auth'], function () use ($app) {
        $app->get('', 'CidadesController@all');
        $app->get('{id}', 'CidadesController@get');
        $app->post('', 'CidadesController@add');
        $app->put('{id}', 'CidadesController@put');
        $app->delete('{id}', 'CidadesController@remove');
    });

    $app->group(['prefix'=>'bairro', 'middleware'=> 'auth'], function () use ($app) {
        $app->get('', 'BairrosController@findAll');
        $app->get('{id}', 'BairrosController@get');
        $app->get('{id}/enderecos', 'BairrosController@getEnderecos');
        $app->post('', 'BairrosController@add');
        $app->put('{id}', 'BairrosController@put');
        $app->delete('{id}', 'BairrosController@remove');
    });

    $app->group(['prefix'=>'complemento', 'middleware'=> 'auth'], function () use ($app) {
        $app->get('', 'ComplementosController@all');
        $app->get('{id}', 'ComplementosController@get');
    });
    
    $app->group(['prefix'=>'complemento', 'middleware' => ['auth', 'admin']], function () use ($app) {
        $app->post('', 'ComplementosController@add');
        $app->put('{id}', 'ComplementosController@put');
        $app->delete('{id}', 'ComplementosController@remove');
    });

    $app->group(['prefix'=>'endereco', 'middleware'=> 'auth'], function () use ($app) {
        $app->get('', 'EnderecosController@all');
        $app->get('{id}', 'EnderecosController@get');
        $app->get('{id}/full', 'EnderecosController@getEndereco');
        $app->post('', 'EnderecosController@add');
        $app->put('{id}', 'EnderecosController@put');
        $app->delete('{id}', 'EnderecosController@remove');
    });

    $app->group(['prefix'=>'universidade', 'middleware'=> 'auth'], function () use ($app) {
        $app->get('', 'UniversidadesController@findAll');
        $app->get('{id}', 'UniversidadesController@get');
    });

    $app->group(['prefix'=>'universidade', 'middleware'=> ['auth', 'admin']], function () use ($app) {
        $app->post('', 'UniversidadesController@cadastrarUniversidade');
        $app->put('{id}', 'UniversidadesController@put');
        $app->delete('{id}', 'UniversidadesController@remove');
    });
    
    $app->group(['prefix'=>'imagem', 'middleware'=> 'auth'], function () use ($app) {
        $app->get('', 'ImagemsController@all');
        $app->get('{id}', 'ImagemsController@get');
        $app->post('', 'ImagemsController@UploadImages');
        $app->put('{id}', 'ImagemsController@put');
        $app->delete('{id}', 'ImagemsController@remove');
    });

    $app->group(['prefix'=>'tipo-republica', 'middleware'=> 'auth'], function () use ($app) {
        $app->get('', 'TipoRepublicasController@all');
        $app->get('{id}', 'TipoRepublicasController@get');
    });

    $app->group(['prefix'=>'tipo-republica', 'middleware'=> ['auth', 'admin']], function () use ($app) {
        $app->post('', 'TipoRepublicasController@add');
        $app->put('{id}', 'TipoRepublicasController@put');
        $app->delete('{id}', 'TipoRepublicasController@remove');
    });

    $app->group(['prefix'=>'tipo-telefone', 'middleware'=> 'auth'], function () use ($app) {
        $app->get('', 'TipoTelefonesController@all');
        $app->get('{id}', 'TipoTelefonesController@get');
    });

    $app->group(['prefix'=>'tipo-telefone', 'middleware'=> ['auth', 'admin']], function () use ($app) {
        $app->post('', 'TipoTelefonesController@add');
        $app->put('{id}', 'TipoTelefonesController@put');
        $app->delete('{id}', 'TipoTelefonesController@remove');
    });

    $app->group(['prefix'=>'republica', 'middleware'=> 'auth'], function () use ($app) {
        // Busca todas as repúblicas com os dados e a localização no mapa
        $app->get('', 'RepublicasController@getAll');
        // Busca todas as Republicas
        $app->get('/buscar', 'RepublicasController@buscarRepublica');
        // Busca todas as vagas apenas com os usuarios
        $app->get('/buscar/users/{id}', 'RepublicasController@getVagasCandidatos');
        // Busca todas as vagas de uma república com usuarios e demais dependencias
        $app->get('/buscar/{id}/vagas', 'RepublicasController@getAllVagas');
        // Busca Todas as repúblicas de um usuário
        $app->get('/user/{id}', 'RepublicasController@getAllByUser');
        // Busca uma república pelo seu ID
        $app->get('{id}', 'RepublicasController@get');
        // Busca todos os moradores da república
        $app->get('/{id}/moradores', 'RepublicasController@getAllMoradores');
        // Cria uma nova república
        $app->post('', 'RepublicasController@cadastrarRepublica');
        // Adiciona um morador na república
        $app->post('/addMorador', 'RepublicasController@addMorador');
        // Remove um morador na república
        $app->post('/removeMorador', 'RepublicasController@removeMorador');
        // Altera uma república pelo seu ID
        $app->put('{id}', 'RepublicasController@put');
        //  Remove uma república pelo seu ID
        $app->delete('{id}', 'RepublicasController@removerRepublica');
    });

    $app->group(['prefix'=>'vaga', 'middleware'=> 'auth'], function () use ($app) {
        // Retorna todas as vagas
        $app->get('', 'VagasController@all');
        // Busca vagas (pode conter parametros para pesquisa)
        $app->get('buscar', 'VagasController@buscarVagas');
        // Retorna uma vaga
        $app->get('{id}', 'VagasController@get');
        // Busca todos candidatos de uma vaga
        $app->get('{id}/candidatos', 'VagasController@getCandidatos');
        // Retorna a quantidade de candidatos que a vaga possui
        $app->get('{id}/applies', 'VagasController@getCountApplies');
        // Retorna true se o usuario tiver candidatado a vaga, false caso contrário
        $app->get('{id}/usuario/{user_id}', 'VagasController@userIsApplied');
        // Cadastra uma nova vaga
        $app->post('', 'VagasController@cadastrarVaga');
        // Atualiza uma vaga
        $app->put('{id}', 'VagasController@put');
        // Muda o estado de uma vaga se tiver ativa, desativa e se tiver desativada ativa
        $app->put('/{id}/estado', 'VagasController@changeState');
        // Atualiza a situação de uma vaga
        $app->put('/updateNotify/{id}', 'VagasController@receivedNotification');
        // Apaga uma vaga
        $app->delete('{id}', 'VagasController@deleteVaga');
    });

    $app->group(['prefix' => 'visita', 'middleware'=> 'auth'], function () use ($app) {
        // Pega todas as visitas
        $app->get('', 'VisitasController@all');
        // 
        $app->get('republica/{id}', 'VisitasController@getAllRepublicVisits'); // Get all by republic grouped
        // Busca uma unica visita
        $app->get('{id}', 'VisitasController@get');
        // Adiciona uma nova visita
        $app->post('', 'VisitasController@agendarVisita');
        // Altera uma visita para visualizada
        $app->put('visualizar/{id}', 'VisitasController@atualizarVisualizacao');
        // Altera uma visita para visualizada
        $app->put('finalizar/{id}', 'VisitasController@finalizarVisita');
        // Remove uma visita
        $app->delete('{id}', 'VisitasController@remove');
    });

    $app->group(['prefix'=>'conveniencia', 'middleware'=> 'auth'], function () use ($app) {
        $app->get('', 'ConvenienciasController@all');
        $app->get('{id}', 'ConvenienciasController@get');
    });

    $app->group(['prefix'=>'conveniencia', 'middleware'=> ['auth', 'admin']], function () use ($app) {
        $app->post('', 'ConvenienciasController@add');
        $app->put('{id}', 'ConvenienciasController@put');
        $app->delete('{id}', 'ConvenienciasController@remove');
    });

    $app->group(['prefix'=>'tipo-auth', 'middleware' => 'auth'], function () use ($app) {
        $app->get('', 'TipoAuthController@all');
        $app->get('{id}', 'TipoAuthController@get');
    });

    $app->group(['prefix'=>'tipo-auth', 'middleware' => ['auth', 'admin']], function () use ($app) {
        $app->post('', 'TipoAuthController@add');
        $app->put('{id}', 'TipoAuthController@put');
        $app->delete('{id}', 'TipoAuthController@remove');
    });

    $app->group(['prefix' => 'despesa', 'middleware' => 'auth'], function () use ($app) {
        $app->get('', 'DespesasController@all');
        $app->get('{id}', 'DespesasController@get');
    });

    $app->group(['prefix' => 'despesa', 'middleware' => ['auth', 'admin']], function () use ($app) {
        $app->post('', 'DespesasController@add');
        $app->put('{id}', 'DespesasController@put');
        $app->delete('{id}', 'DespesasController@remove');
    });

    
});


$app->get('/', function () use ($app) {
    return "Api-Documentation for APP iHaus 2017";
});

$app->group(['prefix' => 'password'], function () use ($app) {
    $app->get('/{token}', function($token) use ($app) {
        header('Location: ihaus://ihaus.com.br/password/reset-password/'.$token);
        exit;
    });
});